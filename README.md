#ESQUELETO PARA EL PROYECTO DE TURISMO DEL CURSO RUBY ON RAILS(version beta produccion)

#Asegurese que tiene la base de datos de postgresql creada con sus respectivos usuarios y privilegios


1. Clone el repositorio con: git clone https://andresmfranco@bitbucket.org/cursorails/appturismo.git

2. Usando el command prompt vaya a la carpeta del repositorio y corra el comando: bundle install

3. Corra la migracion de la base de datos en entorno de desarrollo : db:migrate RAILS_ENV=development

4. Edite el archivo seeds.rb que esta en /db/seeds.rb , quite los comentarios de tal forma de que quede estecodigo habilitado:

      connection = ActiveRecord::Base.connection
      connection.tables.each do |table|
      connection.execute("TRUNCATE #{table} RESTART IDENTITY CASCADE") unless table == "schema_migrations"
      end

      Admin.create([{ name: 'admin',nickname:'admin',email:'administrator@admin.com',password:'password1',password_confirmation:'password1'}])


      sql = File.read('db/init_script.sql')
      statements = sql.split(/;$/)
      statements.pop

      ActiveRecord::Base.transaction do
      statements.each do |statement|
      connection.execute(statement)
      end
      end

5. Ejecute el comando: rake RAILS_ENV=development db:seed 
Este comando truncara todas las tablas y ejecutara el seeds.rb junto con el script init_script.sql y creara el usuari administrador
con los permisos necesarios.
solo ejecute este comando una sola vez y vuelva a dejarlo comentariado como estaba antes.

6. Inicie su aplicacion con el comando : rails server

7. el usuario que debe usar es el que se creo con los seeds: 
usuario: administrator@admin.com
password:'password1'