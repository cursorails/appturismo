# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
previewImages = ->
  $preview = $('#preview').empty()

  readAndPreview = (i, file) ->
    if !/\.(jpe?g|png|gif)$/i.test(file.name)
      return
    # else...
    reader = new FileReader
    $(reader).on 'load', ->
      $preview.append $('<img/>',
        src: @result
        height: 60)
      return
    reader.readAsDataURL file
    return

  if @files
    $.each @files, readAndPreview
  return

$('#file-input').on 'change', previewImages