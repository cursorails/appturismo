class ApplicationController < ActionController::Base
 before_action :authenticate_admin!
 before_action :configure_permitted_parameters, if: :devise_controller?
  include Pundit
  layout :layout_by_resource
  protect_from_forgery with: :null_session
  before_filter :set_constants

  def set_constants
      @product_type = ProductType.find_by_name("Tour")
      @popular_tours  = Product.where(is_popular:"yes",product_type_id:@product_type.id)
    end
  def hello
    render html: "¡Hola, mundo!"
  end
  def goodbye
    render html: "goodbye, world!"
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:username])
  end

  def layout_by_resource
   
    if devise_controller?
     
      'application'
       
   
      if resource_name == :admin && action_name =="edit"
        'admin_lte_2'
      
       end
    else
      
        'admin_lte_2'
      
     end
   
  end
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

private

def user_not_authorized
  flash[:warning] = "You are not authorized to perform this action."
  redirect_to(request.referrer || root_path)
end
def current_user
  current_user =current_admin
end
end
