class ComponentActionsController < ApplicationController
  before_action :set_component_action, only: [:show, :edit, :update, :destroy,:get_roles_by_actions]

  # GET /component_actions
  # GET /component_actions.json
  def index
      
    @component_actions = ComponentAction.all
  end

  # GET /component_actions/1
  # GET /component_actions/1.json
  def show
    authorize @component_action
  end

  # GET /component_actions/new
  def new
    @component_action = ComponentAction.new
    @role =Role.all
    @system_component=SystemComponent.all
    @system_action =SystemAction.all
    
  end

  # GET /component_actions/1/edit
  def edit
    
    @selected_role=@component_action.role_id
    @selected_system_component=@component_action.system_component_id
    @selected_system_action =@component_action.system_action_id
    @role =Role.all
    @system_component=SystemComponent.all
    @system_action =SystemAction.all
  end

  # POST /component_actions
  # POST /component_actions.json
  def create
    @component_action = ComponentAction.new(component_action_params)
    authorize @component_action
    respond_to do |format|
      if @component_action.save
        format.html { redirect_to @component_action, notice: 'Component action was successfully created.' }
        format.json { render :show, status: :created, location: @component_action }
      else
      
    @role =Role.all
    @system_component=SystemComponent.all
    @system_action =SystemAction.all
    @selected_role=component_action_params[:role_id]
    @selected_system_component=component_action_params[:selected_system_component_id]
    @selected_system_action =component_action_params[:selected_system_action_id]
    
        format.html { render :new }
        format.json { render json: @component_action.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /component_actions/1
  # PATCH/PUT /component_actions/1.json
  def update
    authorize @component_action
    respond_to do |format|
      if @component_action.update(component_action_params)
        format.html { redirect_to @component_action, notice: 'Component action was successfully updated.' }
        format.json { render :show, status: :ok, location: @component_action }
      else
    @role =Role.all
    @system_component=SystemComponent.all
    @system_action =SystemAction.all
    @selected_role=component_action_params[:role_id]
    @selected_system_component=component_action_params[:selected_system_component_id]
    @selected_system_action =component_action_params[:selected_system_action_id]
        format.html { render :edit }
        format.json { render json: @component_action.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /component_actions/1
  # DELETE /component_actions/1.json
  def destroy
    authorize @component_action
    @component_action.destroy
    respond_to do |format|
      format.html { redirect_to component_actions_url, notice: 'Component action was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
   def get_roles_by_actions(action_name,component_name)
        @system_action =SystemAction.find_by_action(action_name) 
        @system_component =SystemComponent.find_by_name(component_name)
        @permitted_roles = ComponentAction.select("roles.role as rolename").joins("INNER JOIN roles ON roles.id = component_actions.role_id").where(system_action_id:@system_action.id,system_component_id:@system_component.id)
        @role_list =[]
        @permitted_roles.each do |permitted_role|
            @role_list.push(permitted_role.rolename)
          end
      return @role_list
    end  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_component_action
      @component_action = ComponentAction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def component_action_params
      params.require(:component_action).permit(:system_component_id, :system_action_id, :role_id)
    end

    
end
