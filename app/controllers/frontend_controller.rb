class FrontendController < ApplicationController
 before_action :authenticate_admin!, except: [ :index ,:search_tour,:tours,:all_tours,:all_hotels,:all_flights,:all_packages,:all_shuttles,:contact_us,:search_hotel,:search_package,:reserve_product,:user_login]
 layout 'front_end'

  def index
    @title ="Tours Populares"
    @countries =Country.all
    @cities =City.select("cities.id as id,cities.name as name,concat(cities.name, ',', countries.nicename) as country_name")
    .joins('INNER JOIN countries on countries.id =cities.country_id')
  end

  def user_login
    redirect_to '/' if current_admin
    @product_type = ProductType.find_by_name("Tour")
    @popular_tours  = Product.where(is_popular:"yes",product_type_id:@product_type.id)
  end

  def get_popular_tours
   @popular_tours  = Product.where(is_popular:"yes")
    return @popular_tours
   
  end 

  def get_product_type_by_name(name)
    @product_type = ProductType.where(name:"Tour")
    return @product_type.id
  end 

  def search_tour
 
     @search_tours=Product.joins(:product_types).where(arrival_country:params[:arrival_country],arrival_city:params[:arrival_city]).where("product_types.name='Tour'")
     render :json => @search_tours
    
    # redirect_to "/tours",search_tours:@search_tours
  end

  def search_hotel
      @product_type = ProductType.find_by_name("Hotel") 
      @title ="Hoteles Populares"
      @popular_tours  = Product.where(is_popular:"yes",product_type_id:@product_type.id)
      if request.post?
         
      arrival_date = params[:arrival_date][:month].to_s+"-"+params[:arrival_date][:day].to_s+"-"+params[:arrival_date][:year].to_s 
      departure_date = params[:departure_date][:month].to_s+"-"+params[:departure_date][:day].to_s+"-"+params[:departure_date][:year].to_s
      
       if params[:hotel_city][:city].to_i == 0
        @search_hotels =Product.joins("INNER JOIN product_types on product_types.id = products.product_type_id")
      .where("product_types.name='Hotel'")
       else
         @search_hotels=Product.joins("INNER JOIN product_types on product_types.id = products.product_type_id")
      .where("product_types.name='Hotel' 
      and ((products.arrival_date ='"+ arrival_date.to_s+"')
      or(products.departure_date = '"+departure_date.to_s+"')
      or(products.hotel_city = '"+params[:hotel_city][:city].to_i.to_s+"')
      )")
       end  
      end
  end
  
  def reserve_product
    
    if admin_signed_in?
    @products =Product.find(params[:product_id])  
    @product_type = ProductType.find_by_name("Tour")
    @popular_tours  = Product.where(is_popular:"yes",product_type_id:@product_type.id)
    @title ="Tours Populares"
    @reservation =Reservation.new
    @reservation.is_front_end ="p";
    @reservation.client_id =current_user.id
    @reservation.status ="Open"
    @reservation.product_id =params[:product_id]
    @reservation.number_adults =0
    @reservation.number_kids=0
    
    else
    redirect_to '/login' 
    end    
    
  end
   
  def reserve_package
    
  end 

  def search_package
    @package_obj =PackagesController.new
    @product_type = ProductType.find_by_name("Tour") 
    @title ="Tours Populares"
    @popular_tours  = Product.where(is_popular:"yes",product_type_id:@product_type.id)
    if request.post?
      if params[:arrival_country][:country].to_i ==0 and params[:arrival_city][:city].to_i ==0
        @packages =Package.all
       else      
    @packages =Package.select("distinct packages.id as id,packages.name as name,packages.description as description,packages.price as price,packages.image as image")
    .joins("inner join package_products on package_products.package_id =packages.id inner join products on package_products.product_id = products.id")
    .where( "products.arrival_country ="+params[:arrival_country][:country].to_i.to_s+" or products.hotel_country="+params[:arrival_country][:country].to_i.to_s+
    " or products.arrival_city="+params[:arrival_city][:city].to_i.to_s+ "or products.hotel_city="+params[:arrival_city][:city].to_i.to_s)
     end
     end
=begin
select distinct packages.name ,packages.description,packages.price from packages 
inner join package_products on package_products.package_id =packages.id
inner join products on package_products.product_id = products.id
where (products.id_country = 226 or products.arrival_country =226 or products.hotel_country=226 
or products.arrival_city=1 or products.hotel_city=1 )
=end    
  end

  def tours 
     
     @product_type = ProductType.find_by_name("Tour") 
      @title ="Tours Populares"
      @popular_tours  = Product.where(is_popular:"yes",product_type_id:@product_type.id)
      if request.post?
        if params[:arrival_country][:country].to_i ==0 and params[:arrival_city][:city].to_i ==0
         @search_tours=Product.joins("INNER JOIN product_types on product_types.id = products.product_type_id")
           .where("product_types.name='Tour'")
       else
           @search_tours=Product.joins("INNER JOIN product_types on product_types.id = products.product_type_id")
      .where("product_types.name='Tour' and ((products.arrival_country ="+ params[:arrival_country][:country].to_i.to_s+") or(products.arrival_city ="+params[:arrival_city][:city].to_i.to_s+"))")
        end  
     
      end
  end 

   def all_tours
     @product_type = ProductType.find_by_name("Tour") 
      @title ="Tours Populares"
      @popular_tours  = Product.where(is_popular:"yes",product_type_id:@product_type.id)
     @tours = Product.where(product_type_id:@product_type.id)
  end 

  def all_hotels
     @product_type = ProductType.find_by_name("Hotel") 
     @title ="Hoteles Populares"
     @popular_tours  = Product.where(is_popular:"yes",product_type_id:@product_type.id)
     @hotels = Product.where(product_type_id:@product_type.id)
  end

  def all_flights
    @product_type = ProductType.find_by_name("Flight") 
     @title ="Vuelos Populares"
     @popular_tours  = Product.where(is_popular:"yes",product_type_id:@product_type.id)
     @flights = Product.where(product_type_id:@product_type.id)
  end

  def all_packages
    @package_obj =PackagesController.new
    @product_type = ProductType.find_by_name("Tour") 
    @title ="Tours Populares"
    @popular_tours  = Product.where(is_popular:"yes",product_type_id:@product_type.id)
    @packages =Package.all
   end  

   def all_shuttles
     @product_type = ProductType.find_by_name("Shuttle")
     @title ="Traslados populares"
     @popular_tours  = Product.where(is_popular:"yes",product_type_id:@product_type.id)
     @shuttles = Product.where(product_type_id:@product_type.id)
   end  

   def contact_us
     @product_type = ProductType.find_by_name("Tour") 
    @title ="Tours Populares"
    @popular_tours  = Product.where(is_popular:"yes",product_type_id:@product_type.id)
    end
end
