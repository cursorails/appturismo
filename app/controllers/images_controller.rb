class ImagesController < ApplicationController
  before_action :set_gallery

  def create
    add_more_images(images_params[:images])
      flash[:error] = "Fallo la carga de los archivos , solo son permitidas imagenes en formato: jpg , jpeg , gif , png" unless @gallery.save
    redirect_to :back
  end

  def destroy
    remove_image_at_index(params[:id].to_i)
    flash[:error] = "Failed deleting image" unless @gallery.save
    redirect_to :back
  end

  private

  def set_gallery
    @gallery = Gallery.find(params[:gallery_id])
  end

  def add_more_images(new_images)
    images = @gallery.images 
    images += new_images
    @gallery.images = images
  end

  def remove_image_at_index(index)
    @gallery.images[index].try(:remove!) # delete image from S3
    @gallery['images'].delete_at(index) # remove from images array
    @gallery.save
    @gallery.reload # if you need to reference the new set of images
  end

  def images_params
    params.require(:gallery).permit({images: []}) # allow nested params as array
  end
end
