class PackageProductsController < ApplicationController
  before_action :set_package_product, only: [:show, :edit, :update, :destroy]
 @@package_obj =PackagesController.new
  
  # GET /package_products
  # GET /package_products.json
  def index
    @package_products = PackageProduct.all
  end

  # GET /package_products/1
  # GET /package_products/1.json
  def show
  end

  # GET /package_products/new
  def new
     
    @packages =Package.all
    @products =Product.all
    @package_product = PackageProduct.new
  end

  # GET /package_products/1/edit
  def edit
    @packages =Package.all
    @products =Product.all
    
    @selected_package= @package_product.package_id
    @selected_product=  @package_product.product_id
  end

  # POST /package_products
  # POST /package_products.json
  def create
    @package_product = PackageProduct.new(package_product_params)
    
    respond_to do |format|
      if @package_product.save
         @package = Package.find(package_product_params[:package_id])
         @package.price =@@package_obj.get_package_price(@package.id)
         @package.save
        format.html { redirect_to @package_product, notice: 'Package product was successfully created.' }
        format.json { render :show, status: :created, location: @package_product }
      else
        @packages =Package.all
        @products =Product.all
        @selected_package= package_product_params[:package_id]
        @selected_product= package_product_params[:product_id]
        format.html { render :new }
        format.json { render json: @package_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /package_products/1
  # PATCH/PUT /package_products/1.json
  def update
    respond_to do |format|
      if @package_product.update(package_product_params)
        format.html { redirect_to @package_product, notice: 'Package product was successfully updated.' }
        format.json { render :show, status: :ok, location: @package_product }
      else
        @packages =Package.all
        @products =Product.all
        @selected_package= package_product_params[:package_id]
        @selected_product= package_product_params[:product_id]
        format.html { render :edit }
        format.json { render json: @package_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /package_products/1
  # DELETE /package_products/1.json
  def destroy
    @package_product.destroy
    respond_to do |format|
      format.html { redirect_to package_products_url, notice: 'Package product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_package_product
      @package_product = PackageProduct.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def package_product_params
      params.require(:package_product).permit(:package_id, :product_id)
    end
end
