class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]

  # GET /products
  # GET /products.json
  def index
   
    @products = Product.all
    @obj_product = ProductsController.new
  end

  def get_country_name(country_id) 
    country_name =Country.find(country_id).nicename unless country_id.nil?
    return country_name
  end

  # GET /products/1
  # GET /products/1.json
  def show
   
    @main_country =Country.find(@product.id_country).nicename unless @product.id_country.nil?
    @departure_country =Country.find(@product.departure_country).nicename unless @product.departure_country.nil?
    @departure_city =City.find(@product.departure_city).name unless @product.departure_city.nil?
    @arrival_country =Country.find(@product.arrival_country).nicename unless @product.arrival_country.nil?
    @arrival_city =City.find(@product.arrival_city).name unless @product.arrival_city.nil?
    @hotel_country =Country.find(@product.hotel_country).nicename unless @product.hotel_country.nil?
    @hotel_city =City.find(@product.hotel_city).name unless @product.hotel_city.nil?

  end

  # GET /products/new
  def new
    @countries =Country.all
    @cities =City.all
    @product_type =ProductType.all
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
    @countries =Country.all
    @cities =City.all
    @product_type =ProductType.all
    @product =Product.find(params[:id])
    @selected_product_type  = @product.product_type_id
    @selected_country = @product.id_country
    @selected_departure_country =@product.departure_country
    @selected_departure_city =@product.departure_city
    @selected_arrival_country =@product.arrival_country
    @selected_arrival_city =@product.arrival_city
    @selected_flight_class =@product.flight_class
    @selected_travel_type =@product.travel_type
    @selected_hotel_country =@product.hotel_country
    @selected_hotel_city =@product.hotel_city
  
    
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        @countries =Country.all
        @cities =City.all
        @product_type =ProductType.all
         @selected_product_type =product_params[:product_type_id]
         @selected_departure_country =product_params[:departure_country]
         @selected_departure_city =product_params[:departure_city]
         @selected_arrival_country =product_params[:arrival_country]
          @selected_arrival_city =product_params[:arrival_city]
          @selected_flight_class =product_params[:flight_class]
          @selected_travel_type =product_params[:travel_type]
          @selected_hotel_country =product_params[:hotel_country]
          @selected_hotel_city =product_params[:hotel_city]
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:name, :description, :id_country, :image, :price,:product_type_id,:departure_date,:arrival_date,:hotel_country,:hotel_city,:departure_country,:departure_city,:arrival_country,:arrival_city,:flight_class,:travel_type,:is_popular)
    end
end
