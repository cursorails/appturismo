class RegistrationsController < Devise::RegistrationsController
layout 'front_end'

  def sign_up_params
    params.require(:admin).permit(:name, :nickname, :email, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:admin).permit(:name, :nickname, :email, :password, :password_confirmation)
  end
   
  # POST /resource
  
  
def response_to_sign_up_failure(resource)
  if resource.email == "" && resource.password == nil
    redirect_to '/register', notice: "Por favor ingrese los datos"
  elsif Admin.pluck(:email).include? resource.email
    redirect_to '/register', notice: "El email ingresado ya existe"
  end
  
end

  def assign_client_role(user_id)
    
    assignments = Assignment.new
    assignments.role_id = 3
    assignments.admin_id = user_id
    assignments.save

  end 

   def after_sign_up_path_for(resource)
      assign_client_role resource.id 
      send_email resource.id
  end
   
   def send_email(user_id)
   @user = Admin.find(user_id)

  # Llamamos al   ActionMailer que creamos
  ActionEmailMailer.welcome_email( @user).deliver

end


end