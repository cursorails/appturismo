class ReservationPackagesController < ApplicationController
  before_action :set_reservation_package, only: [:show, :edit, :update, :destroy]

  # GET /reservation_packages
  # GET /reservation_packages.json
  def index
    @reservation_packages = ReservationPackage.all
  end

  # GET /reservation_packages/1
  # GET /reservation_packages/1.json
  def show
  end

  # GET /reservation_packages/new
  def new
    @reservations = Reservation.all
    @packages =Package.all
    @reservation_package = ReservationPackage.new
  end

  # GET /reservation_packages/1/edit
  def edit
    @reservations = Reservation.all
    @packages =Package.all
    @selected_reservation = @reservation_package.reservation_id
    @selected_package = @reservation_package.package_id
    @packages =Package.all
  end

  # POST /reservation_packages
  # POST /reservation_packages.json
  def create
    @reservation_package = ReservationPackage.new(reservation_package_params)

    respond_to do |format|
      if @reservation_package.save
        format.html { redirect_to @reservation_package, notice: 'Reservation package was successfully created.' }
        format.json { render :show, status: :created, location: @reservation_package }
      else
        format.html { render :new }
        format.json { render json: @reservation_package.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reservation_packages/1
  # PATCH/PUT /reservation_packages/1.json
  def update
    respond_to do |format|
      if @reservation_package.update(reservation_package_params)
        format.html { redirect_to @reservation_package, notice: 'Reservation package was successfully updated.' }
        format.json { render :show, status: :ok, location: @reservation_package }
      else
        format.html { render :edit }
        format.json { render json: @reservation_package.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservation_packages/1
  # DELETE /reservation_packages/1.json
  def destroy
    @reservation_package.destroy
    respond_to do |format|
      format.html { redirect_to reservation_packages_url, notice: 'Reservation package was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation_package
      @reservation_package = ReservationPackage.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservation_package_params
      params.require(:reservation_package).permit(:package_id, :reservation_id, :integer)
    end
end
