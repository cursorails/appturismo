class ReservationProductsController < ApplicationController
  before_action :set_reservation_product, only: [:show, :edit, :update, :destroy]

  # GET /reservation_products
  # GET /reservation_products.json
  def index
    @reservation_products = ReservationProduct.all
  end

  # GET /reservation_products/1
  # GET /reservation_products/1.json
  def show
   
  end

  def get_client_name_by_id(id)
     @client_name = Admin.find(id).name
  end

  # GET /reservation_products/new
  def new
    @reservation_product = ReservationProduct.new
    @reservations =Reservation.all
    @products =Product.all
  end

  # GET /reservation_products/1/edit
  def edit
      @reservations =Reservation.all
    @products =Product.all
     @selected_product=@reservation_product.product_id
      @selected_reservation =@reservation_product.reservation_id
  end

  # POST /reservation_products
  # POST /reservation_products.json
  def create
    @reservation_product = ReservationProduct.new(reservation_product_params)

    respond_to do |format|
      if @reservation_product.save
        format.html { redirect_to @reservation_product, notice: 'Reservation product was successfully created.' }
        format.json { render :show, status: :created, location: @reservation_product }
      else
        format.html { render :new }
        format.json { render json: @reservation_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reservation_products/1
  # PATCH/PUT /reservation_products/1.json
  def update
    respond_to do |format|
      if @reservation_product.update(reservation_product_params)
        format.html { redirect_to @reservation_product, notice: 'Reservation product was successfully updated.' }
        format.json { render :show, status: :ok, location: @reservation_product }
      else
        format.html { render :edit }
        format.json { render json: @reservation_product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservation_products/1
  # DELETE /reservation_products/1.json
  def destroy
    @reservation_product.destroy
    respond_to do |format|
      format.html { redirect_to reservation_products_url, notice: 'Reservation product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation_product
      @reservation_product = ReservationProduct.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservation_product_params
      params.require(:reservation_product).permit(:product_id, :reservation_id)
    end
end
