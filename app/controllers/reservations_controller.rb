class ReservationsController < ApplicationController
  before_action :set_reservation, only: [:show, :edit, :update, :destroy,:my_reservation]
  @@package_obj =PackagesController.new
  
  # GET /reservations
  # GET /reservations.json
  def index
   @obj =self

   @roles = Role.select('roles.role as role_name').joins(:assignments).where('assignments.admin_id ='+current_user.id.to_s) 
   permited_roles=[]
   has_role =false
   @roles.each  do |role|
    if role.role_name =='client'
     has_role =true  
   end
   end
   if has_role ==true
      @reservations = Reservation.where(client_id:current_user.id) 
      
   else
      @reservations = Reservation.all
   end    
  end

 

  # GET /reservations/1
  # GET /reservations/1.json
  def show
   @obj =self
   @total=get_reservation_amount(@reservation)
   @reservation.amount =@total.to_d
   @reservation.save
  end

  def get_reservation_amount(reservation)
  
   @reservation_products = ReservationProduct.where(reservation_id:reservation.id)
   @reservation_packages = ReservationPackage.where(reservation_id:reservation.id)
   @tax =Parameter.find_by_name("tax").value
   @percentage_discount_kids= Parameter.find_by_name("percentage_discount_kids").value
     
   
   @kids_product_total = discount_kids(get_reservation_product_price(reservation.id).to_f,reservation.number_kids.to_i ,@percentage_discount_kids.to_i)
   @adult_product_total = get_reservation_product_price(reservation.id).to_f * reservation.number_adults.to_i 
   total_products =@kids_product_total+ @adult_product_total
   
   @kids_package_total = discount_kids(get_reservation_package_price(reservation.id).to_f,reservation.number_kids.to_i ,@percentage_discount_kids.to_i)
   @adult_package_total = get_reservation_package_price(reservation.id).to_f * reservation.number_adults.to_i
   total_packages =  @kids_package_total+@adult_package_total
   @total_products =total_products
   @total_packages =total_packages
   total_amount=total_products+total_packages
   @total =get_value_with_tax(total_amount,@tax.to_i)

   return @total
   
  end
   

  def get_amount(reservation_id,number_kids,number_adults)
  
   reservation_products = ReservationProduct.where(reservation_id:reservation_id)
   reservation_packages = ReservationPackage.where(reservation_id:reservation_id)
   tax =Parameter.find_by_name("tax").value
   percentage_discount_kids= Parameter.find_by_name("percentage_discount_kids").value
    
  
   kids_product_total = discount_kids(get_reservation_product_price(reservation_id).to_f,number_kids.to_i ,percentage_discount_kids.to_i)
   adult_product_total = get_reservation_product_price(reservation_id).to_f * number_adults.to_i 
   total_products =kids_product_total+ adult_product_total
   
   kids_package_total = discount_kids(get_reservation_package_price(reservation_id).to_f,number_kids.to_i ,percentage_discount_kids.to_i)
   adult_package_total = get_reservation_package_price(reservation_id).to_f * number_adults.to_i
   total_packages =  kids_package_total+adult_package_total
  
   total_amount=total_products+total_packages
   total =get_value_with_tax(total_amount,tax.to_i)

   return  total
   
  end

  def get_value_with_tax(amount,tax)
    price_with_tax =((amount*tax)/100)+amount
    return  price_with_tax
  end  

  def discount_kids(amount,kids_number,percentage_discount)
     total =(amount * kids_number)-(((amount * kids_number)*percentage_discount)/100)
     return total
  end

   def get_reservation_product_price(reservation_id)
    @product_info = Product.select("coalesce(sum(products.price),0) AS price ").joins(:reservation_products).where('reservation_products.reservation_id='+reservation_id.to_s)
    @product_info.each do |product|
     @product_price = product.price
    end   
   return @product_price
  end
  
   def get_reservation_package_price(reservation_id)

    package_info = Package.joins(:reservation_packages).where('reservation_packages.reservation_id='+reservation_id.to_s)
    package_info.each do |package|
     @package_price = @@package_obj.get_package_price(package.id)
    end   
   return @package_price
  end 

  def get_total_amount(products_price,packages_price,tax)
    @total =products_price+packages_price+tax
  end  

   def add_product_temp
       @package=Package.find(params[:product_id])
       @package.each do |package|
       @reservation_products.push("product_id"=>package.id,"product"=>package.name,"type"=>package.package_type_id)
        
    end  
      redirect_to new_reservation_path
   end  
  # GET /reservations/new
  def new
    @reservation_products =[]
    get_clients
    @reservation = Reservation.new
  end

  def get_clients
    @clients = Admin.select("admins.id as id ,admins.name as name").joins(:assignments,:roles).where("roles.role='client'")
=begin
select name from admins inner join assignments on admins.id =assignments.admin_id 
inner join roles on roles.id = assignments.role_id
where roles.role ='client'
=end
  end

  def get_client_name_by_id(id)
     @client_name = Admin.find(id).name
  
  end
  def get_products
   @products =Product.all
  end

  def get_packages
   @packages =Package.all
  end

  # GET /reservations/1/edit
  def edit
    get_clients
    @selected_client =@reservation.client_id
    @selected_status = @reservation.status
  end

  # POST /reservations
  # POST /reservations.json
  def create
    @reservation = Reservation.new(reservation_params)
   
    respond_to do |format|
      if @reservation.save
        if @reservation.is_front_end =="p" 
          
        

        @reservation_product =ReservationProduct.new
        @reservation_product.reservation_id =@reservation.id
        @reservation_product.product_id =@reservation.product_id
        @reservation_product.save
          
        @reservation_update =Reservation.find(@reservation.id)   
        @reservation_update.name = generate_reservation_name(@reservation.product_id,@reservation)
        @reservation_update.reservation_date =@reservation.created_at
        @reservation_update.amount =get_amount(@reservation_update.id,@reservation_update.number_kids,@reservation_update.number_adults).to_f
        @reservation_update.save
        send_email(@reservation_update.client_id,@reservation_update,@reservation.product_id)
          format.html { redirect_to '/reserve_product/'+@reservation.product_id.to_s, notice: 'Su reservacion se ha realizado satisfactoriamente' } 
         else
        format.html { redirect_to @reservation, notice: 'Reservation was successfully created.' }
        format.json { render :show, status: :created, location: @reservation }
        end  
        
      else
         if @reservation.is_front_end =="p"
          format.html { redirect_to '/reserve_product/'+@reservation.product_id.to_s,@reservation.errors }
          format.json { render json: @reservation.errors, status: :unprocessable_entity } 
         else  
        format.html { render :new }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
        end
      end
    end
  end
 def send_email(user_id,reservation,product_id)
   @user = Admin.find(user_id)
   @products = Product.find(product_id)
   taxparam =Parameter.find_by_name('tax')
   @tax = taxparam.value
   discount_kids_param =Parameter.find_by_name('percentage_discount_kids')
   @discount_kids=discount_kids_param.value

  # Llamamos al   ActionMailer que creamos
  ActionEmailMailer.reservation_email( @user,reservation,@products,@tax,@discount_kids).deliver

  end
def generate_reservation_name(product_id,reservation)
    
    product = Product.find(product_id).name
    customer_name =Admin.find(reservation.client_id).name
    created_at = reservation.created_at.strftime("%Y-%m-%d").to_s+'-'+reservation.created_at.strftime("%I:%M%p").to_s

     
    
    reservation_name = created_at+'-'+product.to_s+'-'+customer_name
    
    return  reservation_name
end
  # PATCH/PUT /reservations/1
  # PATCH/PUT /reservations/1.json
  def update
    respond_to do |format|
      if @reservation.update(reservation_params)
        format.html { redirect_to @reservation, notice: 'Reservation was successfully updated.' }
        format.json { render :show, status: :ok, location: @reservation }
      else
        format.html { render :edit }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservations/1
  # DELETE /reservations/1.json
  def destroy
    @reservation.destroy
    respond_to do |format|
      format.html { redirect_to reservations_url, notice: 'Reservation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation
      @reservation = Reservation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservation_params
      params.require(:reservation).permit(:client_id, :name, :description, :reservation_date, :arrival_date, :departure_date, :status, :amount,:number_adults,:number_kids,:is_front_end,:product_id)
    end
end
