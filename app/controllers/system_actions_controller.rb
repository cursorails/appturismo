class SystemActionsController < ApplicationController
  before_action :set_system_action, only: [:show, :edit, :update, :destroy]

  # GET /system_actions
  # GET /system_actions.json
  def index
    @system_actions = SystemAction.all
  end

  # GET /system_actions/1
  # GET /system_actions/1.json
  def show
    authorize @system_action
  end

  # GET /system_actions/new
  def new
    @system_action = SystemAction.new
  end

  # GET /system_actions/1/edit
  def edit
  end

  # POST /system_actions
  # POST /system_actions.json
  def create
    @system_action = SystemAction.new(system_action_params)
   authorize @system_action
    respond_to do |format|
      if @system_action.save
        format.html { redirect_to @system_action, notice: 'System action was successfully created.' }
        format.json { render :show, status: :created, location: @system_action }
      else
        format.html { render :new }
        format.json { render json: @system_action.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /system_actions/1
  # PATCH/PUT /system_actions/1.json
  def update
    authorize @system_action
    respond_to do |format|
      if @system_action.update(system_action_params)
        format.html { redirect_to @system_action, notice: 'System action was successfully updated.' }
        format.json { render :show, status: :ok, location: @system_action }
      else
        format.html { render :edit }
        format.json { render json: @system_action.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /system_actions/1
  # DELETE /system_actions/1.json
  def destroy
    authorize @system_action
    @system_action.destroy
    respond_to do |format|
      format.html { redirect_to system_actions_url, notice: 'System action was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_system_action
      @system_action = SystemAction.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def system_action_params
      params.require(:system_action).permit(:action, :description)
    end
end
