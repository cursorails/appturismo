class SystemComponentsController < ApplicationController
  before_action :set_system_component, only: [:show, :edit, :update, :destroy]

  # GET /system_components
  # GET /system_components.json
  def index
    @system_components = SystemComponent.all
  end

  # GET /system_components/1
  # GET /system_components/1.json
  def show
    authorize  @system_component
  end

  # GET /system_components/new
  def new
    @system_component = SystemComponent.new
  end

  # GET /system_components/1/edit
  def edit
  end

  # POST /system_components
  # POST /system_components.json
  def create
    @system_component = SystemComponent.new(system_component_params)
    authorize  @system_component

    respond_to do |format|
      if @system_component.save
        format.html { redirect_to @system_component, notice: 'System component was successfully created.' }
        format.json { render :show, status: :created, location: @system_component }
      else
        format.html { render :new }
        format.json { render json: @system_component.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /system_components/1
  # PATCH/PUT /system_components/1.json
  def update
    authorize  @system_component
    respond_to do |format|
      if @system_component.update(system_component_params)
        format.html { redirect_to @system_component, notice: 'System component was successfully updated.' }
        format.json { render :show, status: :ok, location: @system_component }
      else
        format.html { render :edit }
        format.json { render json: @system_component.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /system_components/1
  # DELETE /system_components/1.json
  def destroy
    authorize  @system_component
    @system_component.destroy
    respond_to do |format|
      format.html { redirect_to system_components_url, notice: 'System component was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_system_component
      @system_component = SystemComponent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def system_component_params
      params.require(:system_component).permit(:name, :description)
    end
end
