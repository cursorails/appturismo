class UserRolesController < ApplicationController
  before_action :set_user_role, only: [:show, :edit, :update, :destroy]
@@roles =RolesController.new 
@@users =UsersController.new 
  # GET /user_roles
  # GET /user_roles.json
  def index
    @user_roles = UserRole.all
  end

  # GET /user_roles/1
  # GET /user_roles/1.json
  def show
   
  end

  # GET /user_roles/new
  def new
    @user_role = UserRole.new
    @users =@@users.get_all_users
    @roles =@@roles.get_all_roles
  end

  # GET /user_roles/1/edit
  def edit
    @users =@@users.get_all_users
    @roles =@@roles.get_all_roles
    @user_roles =UserRole.find(params[:id])
    @selected_role=@user_roles.role_id
    @selected_user=@user_roles.user_id  
end

  # POST /user_roles
  # POST /user_roles.json
  def create
    @user_role = UserRole.new(user_role_params)
    respond_to do |format|
      if @user_role.save
        format.html { redirect_to user_path(User.find(user_role_params[:userid])), notice: 'User role was successfully created.' }
        format.json { render :show, status: :created, location: @user_role }
      else
        format.html { render :new }
        format.json { render json: @user_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /user_roles/user_id
  # POST /user_roles.json
  def create_role_for_user
    @user_role = UserRole.new(user_role_params)
    respond_to do |format|
      if @user_role.save
        format.js {render inline: "location.reload();" }
        format.html { redirect_to '/users/'+user_role_params[:user_id], notice: 'User role was successfully created.' }
        #format.json { render :show, status: :created, location: @user_role }
      else
        format.js {render inline: "location.reload();" }
          format.html { redirect_to '/users/'+user_role_params[:user_id]}
         format.json { render json: @user_role.errors, status: :unprocessable_entity }
      end
    end
  end



  # PATCH/PUT /user_roles/1
  # PATCH/PUT /user_roles/1.json
  def update
    respond_to do |format|
      if @user_role.update(user_role_params)
        format.html { redirect_to @user_role, notice: 'User role was successfully updated.' }
        format.json { render :show, status: :ok, location: @user_role }
      else
        format.html { render :edit }
        format.json { render json: @user_role.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /user_roles/1
  # DELETE /user_roles/1.json
  def destroy
    @user_role.destroy
    respond_to do |format|
      format.html { redirect_to user_roles_url, notice: 'User role was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def get_user_role_by_userid(user_id)
    return UserRole.where(user_id:user_id)
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_role
      @user_role = UserRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_role_params
      params.require(:user_role).permit(:role_id,:user_id)
    end
end
