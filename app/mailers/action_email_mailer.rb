class ActionEmailMailer < ApplicationMailer
    #default from: 'appturismo@outlook.com'
    default from: 'info@appsamf.online'

def welcome_email(user)
  @user = user
  mail(to: @user.email, subject: 'Registro en Agencia de Viajes etc')
end

def reservation_email(user,reservation,products,tax,discount_kids)
  @user = user
  @reservation =reservation
  @products =products
  @tax =tax
  @discount_kids=discount_kids
  mail(to: @user.email, subject: 'Reservacion de su viaje Agencia de Viajes ETC')
end

end

