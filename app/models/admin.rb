class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable


  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
 has_many :assignments
 has_many :roles, through: :assignments
 validates :name, presence: true
#validates :name ,presence: true

def role?(role)
  roles.any? { |r| r.role.underscore.to_sym == role }
end

def multiple_role?(permited_roles)
  
   permited_roles.each {|pr|
    if  roles.any? { |r| r.role == pr}
       return  true
   end   
   }
  return false
end
end
