class Assignment < ApplicationRecord
  belongs_to :admin
  belongs_to :role
  validates_uniqueness_of :role_id, :scope => :admin_id
end
