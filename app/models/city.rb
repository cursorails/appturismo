class City < ApplicationRecord
    belongs_to :country
    validates_uniqueness_of :name, :scope => :country_id
    validates :name, presence: true
end
