class ComponentAction < ApplicationRecord
  belongs_to :system_component
  belongs_to :system_action
  belongs_to :role

  validates_uniqueness_of :role_id, :scope => [:system_component_id, :system_action_id]
end
