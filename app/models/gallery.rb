class Gallery < ApplicationRecord
    validates :title, presence: true
    mount_uploaders :images, ImageUploader # mount the uploaders
end
