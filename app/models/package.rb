class Package < ApplicationRecord
    has_one :package_type
    has_many :package_products
    mount_uploader :image, ImageUploader
    has_many :reservation_packages
    has_many :reservations, through: :reservation_packages
    validates :name, presence: true
    validates :package_type_id, presence: true
end
