class PackageProduct < ApplicationRecord
 belongs_to :package
 belongs_to :product
  validates_uniqueness_of :package_id, :scope => :product_id
end
