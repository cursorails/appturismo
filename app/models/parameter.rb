class Parameter < ApplicationRecord
  validates_uniqueness_of :name, :scope => :value
  validates :name, presence: true
  validates :value, presence: true
end
