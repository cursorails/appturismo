class Product < ApplicationRecord
    belongs_to :product_type
    has_many :package_products
    has_many :reservation_products
    has_many :reservations, through: :reservation_products
    mount_uploader :image, ImageUploader
    validates :name, presence: true
    validates :price, presence: true
    validates :product_type_id, presence: true
end
