class ProductType < ApplicationRecord
has_one :product
validates :name, presence: true,uniqueness: true
end
