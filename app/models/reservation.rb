class Reservation < ApplicationRecord
    has_many :reservation_products
    has_many :products, through: :reservation_products
    validates :client_id, presence: true
    validates :status, presence: true
    validates :number_kids, presence: true
    validates :number_adults, presence: true
    attr_accessor :is_front_end
    attr_accessor :product_id
    
end
