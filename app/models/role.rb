class Role < ApplicationRecord
   has_many :assignments
   has_many :admins, through: :assignments 
   validates :role, presence: true,uniqueness: true
end
