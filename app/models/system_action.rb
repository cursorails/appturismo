class SystemAction < ApplicationRecord
     validates :action, presence: true,uniqueness: true
end
