class User < ApplicationRecord
    has_many :user_roles, dependent: :destroy
    validates :name,:password,:email, presence: true
    validates_format_of :email,:with => /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/
end
