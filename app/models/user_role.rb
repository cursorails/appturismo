class UserRole < ApplicationRecord
  belongs_to :role
  belongs_to :user
  validates_uniqueness_of :role_id, :scope => :user_id
end
