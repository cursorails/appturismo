class AdminPolicy < ApplicationPolicy
@@component_action =ComponentActionsController.new
     
   def create?
     permited_roles =@@component_action.get_roles_by_actions("insert","Admin")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end

   def update?
     permited_roles =@@component_action.get_roles_by_actions("update","Admin")
      user.multiple_role? permited_roles
    end


     def destroy?
     permited_roles =@@component_action.get_roles_by_actions("delete","Admin")
      user.multiple_role? permited_roles
    end
     def show?
     permited_roles =@@component_action.get_roles_by_actions("show","Admin")
      user.multiple_role? permited_roles
    end

    def system_admin
      permited_roles =@@component_action.get_roles_by_actions("show","System Administrator")
      user.multiple_role? permited_roles
    end    

     
  
end