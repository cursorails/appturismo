class CountryPolicy < ApplicationPolicy
    @@component_action =ComponentActionsController.new
       
       def create?
        permited_roles =@@component_action.get_roles_by_actions("insert","Country")
         #(user.role? :admin) ||  (user.role? :agent)
         # user.role? :admin
         user.multiple_role? permited_roles
       end 
      def update?
        permited_roles =@@component_action.get_roles_by_actions("update","Country")
         #(user.role? :admin) ||  (user.role? :agent)
         # user.role? :admin
         user.multiple_role? permited_roles
       end
   
         def show?
        permited_roles =@@component_action.get_roles_by_actions("show","Country")
         #(user.role? :admin) ||  (user.role? :agent)
         # user.role? :admin
         user.multiple_role? permited_roles
       end 
   
         def destroy?
        permited_roles =@@component_action.get_roles_by_actions("delete","Country")
         #(user.role? :admin) ||  (user.role? :agent)
         # user.role? :admin
         user.multiple_role? permited_roles
       end 
   end