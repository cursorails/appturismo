class PackagePolicy < ApplicationPolicy
 @@component_action =ComponentActionsController.new
    
    def create?
     permited_roles =@@component_action.get_roles_by_actions("insert","Package")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 
   def update?
     permited_roles =@@component_action.get_roles_by_actions("update","Package")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end

      def show?
     permited_roles =@@component_action.get_roles_by_actions("show","Package")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 

      def destroy?
     permited_roles =@@component_action.get_roles_by_actions("delete","Package")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 
end