class PackageTypePolicy < ApplicationPolicy
 @@component_action =ComponentActionsController.new
    
    def create?
     permited_roles =@@component_action.get_roles_by_actions("insert","PackageType")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 
   def update?
     permited_roles =@@component_action.get_roles_by_actions("update","PackageType")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end

      def show?
     permited_roles =@@component_action.get_roles_by_actions("show","PackageType")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 

      def destroy?
     permited_roles =@@component_action.get_roles_by_actions("delete","PackageType")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 
end