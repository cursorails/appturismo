class ProductPolicy < ApplicationPolicy
 @@component_action =ComponentActionsController.new
    
    def create?
     permited_roles =@@component_action.get_roles_by_actions("insert","Product")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 
   def update?
     permited_roles =@@component_action.get_roles_by_actions("update","Product")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end

      def show?
     permited_roles =@@component_action.get_roles_by_actions("show","Product")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 

      def destroy?
     permited_roles =@@component_action.get_roles_by_actions("delete","Product")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 
end