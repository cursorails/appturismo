class ProductTypePolicy < ApplicationPolicy
 @@component_action =ComponentActionsController.new
    
    def create?
     permited_roles =@@component_action.get_roles_by_actions("insert","ProductType")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 
   def update?
     permited_roles =@@component_action.get_roles_by_actions("update","ProductType")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end

      def show?
     permited_roles =@@component_action.get_roles_by_actions("show","ProductType")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 

      def destroy?
     permited_roles =@@component_action.get_roles_by_actions("delete","ProductType")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 
end