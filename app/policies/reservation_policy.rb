class ReservationPolicy < ApplicationPolicy
 @@component_action =ComponentActionsController.new
    
    def create?
     permited_roles =@@component_action.get_roles_by_actions("insert","Reservation")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 
   def update?
     permited_roles =@@component_action.get_roles_by_actions("update","Reservation")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end

      def show?
     permited_roles =@@component_action.get_roles_by_actions("show","Reservation")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 

      def destroy?
     permited_roles =@@component_action.get_roles_by_actions("delete","Reservation")
      #(user.role? :admin) ||  (user.role? :agent)
      # user.role? :admin
      user.multiple_role? permited_roles
    end 

    def manage_reservation
      permited_roles =@@component_action.get_roles_by_actions("show","Manage Reservation")
      user.multiple_role? permited_roles
    end    
end

