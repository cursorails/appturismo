json.extract! component_action, :id, :system_component_id, :system_action_id, :role_id, :created_at, :updated_at
json.url component_action_url(component_action, format: :json)