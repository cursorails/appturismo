json.extract! package_product, :id, :package_id, :product_id, :created_at, :updated_at
json.url package_product_url(package_product, format: :json)