json.extract! package_type, :id, :name, :description, :created_at, :updated_at
json.url package_type_url(package_type, format: :json)