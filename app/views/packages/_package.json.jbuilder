json.extract! package, :id, :name, :description, :package_type_id, :price, :image, :created_at, :updated_at
json.url package_url(package, format: :json)