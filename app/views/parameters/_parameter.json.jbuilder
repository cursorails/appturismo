json.extract! parameter, :id, :name, :value, :created_at, :updated_at
json.url parameter_url(parameter, format: :json)