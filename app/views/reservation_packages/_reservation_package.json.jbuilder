json.extract! reservation_package, :id, :package_id, :reservation_id, :integer, :created_at, :updated_at
json.url reservation_package_url(reservation_package, format: :json)