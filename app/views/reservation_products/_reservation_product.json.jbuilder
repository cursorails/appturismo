json.extract! reservation_product, :id, :product_id, :reservation_id, :created_at, :updated_at
json.url reservation_product_url(reservation_product, format: :json)