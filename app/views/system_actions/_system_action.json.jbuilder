json.extract! system_action, :id, :action, :description, :created_at, :updated_at
json.url system_action_url(system_action, format: :json)