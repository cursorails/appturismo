json.extract! system_component, :id, :name, :description, :created_at, :updated_at
json.url system_component_url(system_component, format: :json)