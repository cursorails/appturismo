require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Railsapp
  class Application < Rails::Application
    
      config.to_prepare do  
       Devise::SessionsController.layout 'admin_lte_2_login'

      end
      
      config.i18n.default_locale = "es"
      config.action_view.field_error_proc = Proc.new { |html_tag, instance| 
  "<div class=\"has-error control-group error\">#{html_tag}</div>".html_safe
}
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    Dir.glob("#{Rails.root}/app/assets/images/**/").each do |path|
      config.assets.paths << path
    end
  end
end
