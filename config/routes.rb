Rails.application.routes.draw do
  resources :countries
  resources :galleries do
    resources :images, :only => [:create,:destroy]
  end
  resources :reservation_packages
  resources :parameters
  resources :cities
  resources :package_products
  resources :packages
  resources :package_types
  resources :reservation_products
  resources :product_types
  resources :products
  resources :reservations
  resources :component_actions
  resources :system_actions
  resources :system_components
  resources :components
 devise_for :admins, :path => '', :path_names => { :sign_in => "login", :sign_out => "logout", :sign_up => "register",:edit=>"user_profile" },:controllers => { registrations: 'registrations',sessions:'sessions' }
scope "/users" do
  resources :admins
end
devise_scope :admin do
  get '/logout' => "devise/sessions#destroy"
end
authenticated :admin do
   root 'frontend#index' ,as: :authenticated_root
end

  root 'frontend#index'
  
  resources :user_roles

  resources :user_roles do
  collection do
    post 'create_role_for_user' 
  end
end
 resources :assignments
  resources :roles
  resources :products
  resources :frontend
 
 # Front end Routes
 get '/user_login',to: 'frontend#user_login'
 get '/tours' , to: 'frontend#index'
 get '/dashboard' , to: 'dashboard#index'
 post '/search_tour' , to: 'frontend#search_tour'
 post '/index' , to: 'frontend#tours'
 post '/tours' , to: 'frontend#tours'
 
 get '/search_hotel' , to: 'frontend#search_hotel'
 post '/search_hotel' , to: 'frontend#search_hotel'

 get '/search_package' , to: 'frontend#search_package'
 post '/search_package' , to: 'frontend#search_package'

 get '/reserve_product/:product_id',to: 'frontend#reserve_product'

 get '/all_tours' , to: 'frontend#all_tours'
 get '/all_hotels' , to: 'frontend#all_hotels'
 get '/all_flights' , to: 'frontend#all_flights'
 get '/all_packages' , to: 'frontend#all_packages'
 get '/all_shuttles' , to: 'frontend#all_shuttles'
 get '/contact_us' , to: 'frontend#contact_us'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Reservations--------------
  post '/add_product_temp' , to: 'reservation#add_product_temp'
 
  #-------------------------
  resources :users
  get 'goodbye', to: 'application#goodbye'
  
  resources :articles do
    resources :comments
  end
end
