class CreateSystemActions < ActiveRecord::Migration[5.0]
  def change
    create_table :system_actions do |t|
      t.string :action
      t.string :description

      t.timestamps
    end
  end
end
