class CreateComponentActions < ActiveRecord::Migration[5.0]
  def change
    create_table :component_actions do |t|
      t.references :system_component, foreign_key: true
      t.references :system_action, foreign_key: true
      t.references :role, foreign_key: true

      t.timestamps
    end
  end
end
