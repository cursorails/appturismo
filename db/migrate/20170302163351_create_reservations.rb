class CreateReservations < ActiveRecord::Migration[5.0]
  def change
    create_table :reservations do |t|
      
      t.integer :client_id
      t.string :name
      t.string :description
      t.datetime :reservation_date
      t.datetime :arrival_date
      t.datetime :departure_date
      t.string :status
      t.decimal :amount

      t.timestamps
    end
  end
end
