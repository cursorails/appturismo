class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.string :description
      t.integer :id_country
      t.string :image
      t.decimal :price
      t.timestamps
    end
  end
end
