class CreateReservationProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :reservation_products do |t|
      t.integer :product_id
      t.integer :reservation_id

      t.timestamps
    end
  end
end
