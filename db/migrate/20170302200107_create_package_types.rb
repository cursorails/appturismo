class CreatePackageTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :package_types do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
