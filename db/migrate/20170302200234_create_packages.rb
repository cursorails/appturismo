class CreatePackages < ActiveRecord::Migration[5.0]
  def change
    create_table :packages do |t|
      t.string :name
      t.string :description
      t.integer :package_type_id
      t.decimal :price
      t.string :image

      t.timestamps
    end
  end
end
