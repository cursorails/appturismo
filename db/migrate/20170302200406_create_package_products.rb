class CreatePackageProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :package_products do |t|
      t.integer :package_id
      t.integer :product_id

      t.timestamps
    end
  end
end
