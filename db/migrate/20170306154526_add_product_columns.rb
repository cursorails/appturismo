class AddProductColumns < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :is_popular, :string
    add_column :products, :departure_date, :datetime
    add_column :products, :arrival_date, :datetime
    add_column :products, :departure_country, :integer
    add_column :products, :arrival_country, :integer
    add_column :products, :departure_city, :integer
    add_column :products, :arrival_city, :integer
    add_column :products, :flight_class, :string
    add_column :products, :number_kids, :integer
    add_column :products, :number_adults, :integer
    add_column :products, :hotel_country, :integer
    add_column :products, :hotel_city, :integer
    add_column :products, :rooms_number, :integer
    add_column :products, :travel_type, :string
  end
end
