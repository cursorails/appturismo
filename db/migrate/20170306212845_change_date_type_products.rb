class ChangeDateTypeProducts < ActiveRecord::Migration[5.0]
  def change
    change_column :products, :departure_date, :date
    change_column :products, :arrival_date, :date
  end
end
