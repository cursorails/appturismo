class AddImageToProducts < ActiveRecord::Migration[5.0]
  def change
    remove_column :products, :image
    add_column :products, :image, :string
  end
end
