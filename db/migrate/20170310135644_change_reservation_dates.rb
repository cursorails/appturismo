class ChangeReservationDates < ActiveRecord::Migration[5.0]
  def change
      change_column :reservations, :arrival_date, :date
      change_column :reservations, :departure_date, :date
  end
end
