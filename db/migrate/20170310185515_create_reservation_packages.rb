class CreateReservationPackages < ActiveRecord::Migration[5.0]
  def change
    create_table :reservation_packages do |t|
      t.integer :package_id
      t.string :reservation_id
      t.string :integer

      t.timestamps
    end
  end
end
