class ChangeReservationPackages < ActiveRecord::Migration[5.0]
  def change
    change_column :reservation_packages, :reservation_id,'integer USING CAST(reservation_id AS integer)'
    remove_column :reservation_packages, :integer
  end
end
