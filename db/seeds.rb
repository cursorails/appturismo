# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
#####IMPORTANTE: SOLO SE DEBE EJECUTAR UNA VEZ PARA INICIAR LOS DATOS DEL ADMINISTRADOR EN UNA BASE DE DATOS VACIA
#----------Ejecuta el script de creacion de datos inicial------------------------------------------------------------
 connection = ActiveRecord::Base.connection
  connection.tables.each do |table|
  connection.execute("TRUNCATE #{table} RESTART IDENTITY CASCADE") unless table == "schema_migrations"
end

####Crea el usuario admin inicial
Admin.create([{ name: 'admin',nickname:'admin',email:'administrator@admin.com',password:'password1',password_confirmation:'password1'}])
 

sql = File.read('db/init_script.sql')
statements = sql.split(/;$/)
statements.pop
   
  #puts statements
  ActiveRecord::Base.transaction do
   statements.each do |statement|
    connection.execute(statement)
    end
  end
#-----------------Fin del Script-----------------------------------------------------------------------------------
