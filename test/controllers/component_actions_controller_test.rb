require 'test_helper'

class ComponentActionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @component_action = component_actions(:one)
  end

  test "should get index" do
    get component_actions_url
    assert_response :success
  end

  test "should get new" do
    get new_component_action_url
    assert_response :success
  end

  test "should create component_action" do
    assert_difference('ComponentAction.count') do
      post component_actions_url, params: { component_action: { role_id: @component_action.role_id, system_action_id: @component_action.system_action_id, system_component_id: @component_action.system_component_id } }
    end

    assert_redirected_to component_action_url(ComponentAction.last)
  end

  test "should show component_action" do
    get component_action_url(@component_action)
    assert_response :success
  end

  test "should get edit" do
    get edit_component_action_url(@component_action)
    assert_response :success
  end

  test "should update component_action" do
    patch component_action_url(@component_action), params: { component_action: { role_id: @component_action.role_id, system_action_id: @component_action.system_action_id, system_component_id: @component_action.system_component_id } }
    assert_redirected_to component_action_url(@component_action)
  end

  test "should destroy component_action" do
    assert_difference('ComponentAction.count', -1) do
      delete component_action_url(@component_action)
    end

    assert_redirected_to component_actions_url
  end
end
