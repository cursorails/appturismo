require 'test_helper'

class PackageProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @package_product = package_products(:one)
  end

  test "should get index" do
    get package_products_url
    assert_response :success
  end

  test "should get new" do
    get new_package_product_url
    assert_response :success
  end

  test "should create package_product" do
    assert_difference('PackageProduct.count') do
      post package_products_url, params: { package_product: { package_id: @package_product.package_id, product_id: @package_product.product_id } }
    end

    assert_redirected_to package_product_url(PackageProduct.last)
  end

  test "should show package_product" do
    get package_product_url(@package_product)
    assert_response :success
  end

  test "should get edit" do
    get edit_package_product_url(@package_product)
    assert_response :success
  end

  test "should update package_product" do
    patch package_product_url(@package_product), params: { package_product: { package_id: @package_product.package_id, product_id: @package_product.product_id } }
    assert_redirected_to package_product_url(@package_product)
  end

  test "should destroy package_product" do
    assert_difference('PackageProduct.count', -1) do
      delete package_product_url(@package_product)
    end

    assert_redirected_to package_products_url
  end
end
