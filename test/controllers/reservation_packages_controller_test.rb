require 'test_helper'

class ReservationPackagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @reservation_package = reservation_packages(:one)
  end

  test "should get index" do
    get reservation_packages_url
    assert_response :success
  end

  test "should get new" do
    get new_reservation_package_url
    assert_response :success
  end

  test "should create reservation_package" do
    assert_difference('ReservationPackage.count') do
      post reservation_packages_url, params: { reservation_package: { integer: @reservation_package.integer, package_id: @reservation_package.package_id, reservation_id: @reservation_package.reservation_id } }
    end

    assert_redirected_to reservation_package_url(ReservationPackage.last)
  end

  test "should show reservation_package" do
    get reservation_package_url(@reservation_package)
    assert_response :success
  end

  test "should get edit" do
    get edit_reservation_package_url(@reservation_package)
    assert_response :success
  end

  test "should update reservation_package" do
    patch reservation_package_url(@reservation_package), params: { reservation_package: { integer: @reservation_package.integer, package_id: @reservation_package.package_id, reservation_id: @reservation_package.reservation_id } }
    assert_redirected_to reservation_package_url(@reservation_package)
  end

  test "should destroy reservation_package" do
    assert_difference('ReservationPackage.count', -1) do
      delete reservation_package_url(@reservation_package)
    end

    assert_redirected_to reservation_packages_url
  end
end
