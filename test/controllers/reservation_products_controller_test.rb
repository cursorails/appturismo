require 'test_helper'

class ReservationProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @reservation_product = reservation_products(:one)
  end

  test "should get index" do
    get reservation_products_url
    assert_response :success
  end

  test "should get new" do
    get new_reservation_product_url
    assert_response :success
  end

  test "should create reservation_product" do
    assert_difference('ReservationProduct.count') do
      post reservation_products_url, params: { reservation_product: { product_id: @reservation_product.product_id, reservation_id: @reservation_product.reservation_id } }
    end

    assert_redirected_to reservation_product_url(ReservationProduct.last)
  end

  test "should show reservation_product" do
    get reservation_product_url(@reservation_product)
    assert_response :success
  end

  test "should get edit" do
    get edit_reservation_product_url(@reservation_product)
    assert_response :success
  end

  test "should update reservation_product" do
    patch reservation_product_url(@reservation_product), params: { reservation_product: { product_id: @reservation_product.product_id, reservation_id: @reservation_product.reservation_id } }
    assert_redirected_to reservation_product_url(@reservation_product)
  end

  test "should destroy reservation_product" do
    assert_difference('ReservationProduct.count', -1) do
      delete reservation_product_url(@reservation_product)
    end

    assert_redirected_to reservation_products_url
  end
end
