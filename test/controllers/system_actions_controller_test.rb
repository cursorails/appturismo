require 'test_helper'

class SystemActionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @system_action = system_actions(:one)
  end

  test "should get index" do
    get system_actions_url
    assert_response :success
  end

  test "should get new" do
    get new_system_action_url
    assert_response :success
  end

  test "should create system_action" do
    assert_difference('SystemAction.count') do
      post system_actions_url, params: { system_action: { action: @system_action.action, description: @system_action.description } }
    end

    assert_redirected_to system_action_url(SystemAction.last)
  end

  test "should show system_action" do
    get system_action_url(@system_action)
    assert_response :success
  end

  test "should get edit" do
    get edit_system_action_url(@system_action)
    assert_response :success
  end

  test "should update system_action" do
    patch system_action_url(@system_action), params: { system_action: { action: @system_action.action, description: @system_action.description } }
    assert_redirected_to system_action_url(@system_action)
  end

  test "should destroy system_action" do
    assert_difference('SystemAction.count', -1) do
      delete system_action_url(@system_action)
    end

    assert_redirected_to system_actions_url
  end
end
