require 'test_helper'

class SystemComponentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @system_component = system_components(:one)
  end

  test "should get index" do
    get system_components_url
    assert_response :success
  end

  test "should get new" do
    get new_system_component_url
    assert_response :success
  end

  test "should create system_component" do
    assert_difference('SystemComponent.count') do
      post system_components_url, params: { system_component: { description: @system_component.description, name: @system_component.name } }
    end

    assert_redirected_to system_component_url(SystemComponent.last)
  end

  test "should show system_component" do
    get system_component_url(@system_component)
    assert_response :success
  end

  test "should get edit" do
    get edit_system_component_url(@system_component)
    assert_response :success
  end

  test "should update system_component" do
    patch system_component_url(@system_component), params: { system_component: { description: @system_component.description, name: @system_component.name } }
    assert_redirected_to system_component_url(@system_component)
  end

  test "should destroy system_component" do
    assert_difference('SystemComponent.count', -1) do
      delete system_component_url(@system_component)
    end

    assert_redirected_to system_components_url
  end
end
