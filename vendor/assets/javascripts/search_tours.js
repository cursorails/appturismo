$(document).on("submit", "form", function() {
      $.ajax({
    type: 'POST',
    // make sure you respect the same origin policy with this url:
    // http://en.wikipedia.org/wiki/Same_origin_policy
    url: 'http://localhost:3000/search_tour',
    data: { 
        'arrival_country': 14, 
        'arrival_city': 1 // <-- the $ sign in the parameter name seems unusual, I would avoid it
    },
    success: function(data){
        
        
$.each(data, function( key, value ) {
  
   $("#search_result").html('<div class="col-md-4 col-sm-6 fh5co-tours animate-box" data-animate-effect="fadeIn">'+
		'<div href="#"><div class="desc"><span></span><h3>'+value.name+'</h3>'+
		'<span><b>'+value.description+'</b></span>'+
		 '<span class="price">$'+value.price+ 'por persona</span>'+
		  '<a class="btn btn-primary btn-outline" href="#">Reserve ahora<i class="icon-arrow-right22"></i></a>'+
		'</div></div></div>');

}

);
    }
});
});